<!doctype html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Специалисты</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->

</head>

<body>

<!-- Header -->
<?php include('inc/header.inc.php') ?><!-- -->

<div class="wrapper">
    <div class="content">

        <div class="container-fluid">

            <h2>Специалисты</h2>

            <div class="top-bar">
                <ul>
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="поиск по базе">
                            <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Искать</button>
                                    </span>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default" title="Экспортировать"><i class="fa fa-download" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>

            <div class="specialist">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>Email</th>
                        <th>Доступ в систему</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Павлов Алексей</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Орлова Елена</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Смирнов Дмитрий</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Павлов Алексей</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Орлова Елена</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Смирнов Дмитрий</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Павлов Алексей</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Орлова Елена</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Смирнов Дмитрий</td>
                        <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                        <td><a href="mailto:mail@mail.ru">mail@mail.ru</a></td>
                        <td><select class="form-control">
                                <option value="открыт">открыт</option>
                                <option value="закрыт">закрыт</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="text-right">
                    <a data-src="#user-add" href="#" class="btn btn-success  btn-modal" title="Добавить"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Добавить специалиста</a>
                </div>

            </div>

        </div>

    </div>
</div>


<!-- New user -->
<div class="hide">
    <div class="modal-box" id="user-add">
        <div class="modal-title">Добавить специалиста</div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="ФИО специалиста">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input3" placeholder="Телефон">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Email">
            </div>

            <div class="form-group">
                <div class="form-group">
                    <select class="form-control">
                        <option value="-">Доступ в систему</option>
                        <option value="открыт">открыт</option>
                        <option value="закрыт">закрыт</option>
                    </select>
                </div>
            </div>
            <div class="row text-center">
                <button type="submit" class="btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> Добавить</button>
            </div>
        </form>
    </div>
</div>  <!-- -->

<!-- Script -->
<?php include('inc/script.inc.php') ?><!-- -->

</body>
</html>
