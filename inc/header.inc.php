<header class="header">
    <a href="#" class="header-link">
        <span class="header-icon">
            <i class="fa fa-home" aria-hidden="true"></i>
        </span>
        <span class="header-text">CRM</span>
    </a>

</header>

<nav class="menu">

    <div class="menu-user">Администратор</div>
    <ul>
        <li>
            <a href="#">Общее расписание</a>
        </li>
        <li>
            <a href="#">Добавить заказ</a>
        </li>
        <li>
            <a href="#">Клиентская база</a>
        </li>
        <li>
            <a href="#">Добавить специалиста</a>
        </li>
        <li>
            <a href="#">Настройки системы</a>
        </li>
        <li>
            <a href="#">Выход</a>
        </li>
    </ul>
</nav>