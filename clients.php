<!doctype html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Клиенты</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->

</head>

<body>

<!-- Header -->
<?php include('inc/header.inc.php') ?><!-- -->

<div class="wrapper">
    <div class="content">

        <div class="container-fluid">

            <h2>Клиенты</h2>

            <div class="top-bar">
                <ul>
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="поиск по базе">
                            <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Искать</button>
                                    </span>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default" title="Экспортировать"><i class="fa fa-download" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>

            <div class="clients">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>Метро</th>
                        <th>Округ</th>
                        <th>Улица</th>
                        <th>ФИО Пациента</th>
                        <th>Пол пациента</th>
                        <th class="user-diagnosis">Диагноз</th>
                        <th>Стоимость</th>
                        <th>Дата обращения</th>
                        <th>Дата начала</th>
                        <th>Дата окончания</th>
                        <th>Кол-во процедур</th>
                        <th>Ответсвенный специалист</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Павлов Алексей</td>
                            <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                            <td>Пражская</td>
                            <td>ЮАО</td>
                            <td>Чертановская 24</td>
                            <td>Павлов Олег</td>
                            <td>муж</td>
                            <td class="user-diagnosis">Cras enim sem, hendrerit quis mi id.</td>
                            <td>9800 руб.</td>
                            <td>15.03.2017</td>
                            <td>22.03.2017</td>
                            <td>16.04.2017</td>
                            <td>10</td>
                            <td>Иванов Петр</td>
                        </tr>
                        <tr>
                            <td>Иванов Сергей</td>
                            <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                            <td>Пражская</td>
                            <td>ЮАО</td>
                            <td>Чертановская 24</td>
                            <td>Сволова Ольга</td>
                            <td>муж</td>
                            <td class="user-diagnosis">Cras enim sem, hendrerit quis mi id.</td>
                            <td>9800 руб.</td>
                            <td>15.03.2017</td>
                            <td>22.03.2017</td>
                            <td>16.04.2017</td>
                            <td>10</td>
                            <td>Иванов Петр</td>
                        </tr>
                        <tr>
                            <td>Смирнов Дмитрий</td>
                            <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                            <td>Пражская</td>
                            <td>ЮАО</td>
                            <td>Чертановская 24</td>
                            <td>Алекссев Игорь</td>
                            <td>муж</td>
                            <td class="user-diagnosis">Cras enim sem, hendrerit quis mi id.</td>
                            <td>9800 руб.</td>
                            <td>15.03.2017</td>
                            <td>22.03.2017</td>
                            <td>16.04.2017</td>
                            <td>10</td>
                            <td>Иванов Петр</td>
                        </tr>
                        <tr>
                            <td>Демидова Елена</td>
                            <td><a href="tel:+7(927) 555-55-55">+7(927) 555-55-55</a></td>
                            <td>Пражская</td>
                            <td>ЮАО</td>
                            <td>Чертановская 24</td>
                            <td>Протасова полина</td>
                            <td>жен</td>
                            <td class="user-diagnosis">Cras enim sem, hendrerit quis mi id.</td>
                            <td>9800 руб.</td>
                            <td>15.03.2017</td>
                            <td>22.03.2017</td>
                            <td>16.04.2017</td>
                            <td>10</td>
                            <td>Иванов Петр</td>
                        </tr>
                    </tbody>
                </table>

            </div>


            <div class="text-right">
                <a data-src="#user-add" href="#" class="btn btn-success  btn-modal" title="Добавить"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Добавить клиента</a>
            </div>

        </div>

    </div>
</div>


<!-- New user -->
<div class="hide">
    <div class="modal-box" id="user-add">
        <div class="modal-title">Добавить клиента</div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="input1" placeholder="ФИО пациента">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input2" placeholder="Дата рождения">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input3" placeholder="Пол">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input4" placeholder="Диагноз">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input6" placeholder="Стоимость">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input5" placeholder="Дата обращения">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input5" placeholder="Дата начала">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="input5" placeholder="Дата окончания">
            </div>
            <div class="form-group">
                <input type="number" class="form-control" name="input6" placeholder="Количество процедур">
            </div>
            <div class="form-group">
                <select class="form-control">
                    <option value="-">Ответсвенный специалист</option>
                    <option value="Володин Сергей">Володин Сергей</option>
                    <option value="Павлов Дмитрий">Павлов Дмитрий</option>
                    <option value="Матросова Оксана">Матросова Оксан</option>
                </select>
            </div>
            <div class="form-group clearfix">
                <label class="label-left">Нужен ли стол?</label>
                <div class="btn-group pull-right" data-toggle="buttons">
                    <label class="btn btn-default active">
                        <input type="radio" name="options" autocomplete="off" checked> да
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="options"  autocomplete="off"> нет
                    </label>
                </div>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="input6" placeholder="Примечание" rows="3"></textarea>
            </div>
        </form>
    </div>
</div>  <!-- -->

<!-- Script -->
<?php include('inc/script.inc.php') ?><!-- -->

</body>
</html>
