

$(".btn-modal").fancybox({
    Infobar: false,
    buttons: false
});



$('.ibox-toggle').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.ibox');
    box.toggleClass('open');
});



$('.plan-nav a').click(function(e) {
    e.preventDefault();

    var box = $(this).closest('.plan');
    var dir = $(this).attr("data-target");
    var week = box.find('.current');

    if (dir == 'prev') {

        if (week.prev().is('.plan-week')) {
            week.removeClass('current');
            week.prev().addClass('current');
        }
    }

    if (dir == 'next') {

        if (week.next().is('.plan-week')) {
            week.removeClass('current');
            week.next().addClass('current');
        }
    }
});



$('.plan-switch').click(function(e) {
    e.preventDefault();

    $(this).toggleClass('switch');
    var box = $(this).closest('.plan');
    box.toggleClass('switch');
});



